import java.util.HashMap;
import java.util.Map;

/**
 * Classe Tradutor
 * 
 * Hands-on: Tradutor com TDD
 * 
 * TDD � Desenvolvimento de Software Guiado pelo Testes por Instituto Tecnol�gico
 * da Aeron�utica
 * 
 * @author Wagner Galv�o galvao.wagner@outlook.com.br
 * @since 1.0
 * @version 1.0
 *
 */
public class Tradutor {

	private Map<String, String> traducoes = new HashMap<>();
	
	public boolean estaVazia() {
		return traducoes.isEmpty();
	}

	public void adicionarTraducao(String palavra, String traducao) {
		if(traducoes.containsKey(palavra)) {
			traducao = traduzir(palavra) + ", " + traducao;
		};
		traducoes.put(palavra, traducao);
	}

	public String traduzir(String palavra) {
		return traducoes.get(palavra);
	}

	public String traduzirFrase(String frase) {
		String[] palavras = frase.split(" ");
		String fraseTraduzida = "";
		for(String palavra : palavras) {
			String traducao = traduzir(palavra);
			traducao = primeiraTraducao(traducao);
			fraseTraduzida += traducao + " ";
		};
		return fraseTraduzida.trim();
	}

	private String primeiraTraducao(String traducao) {
		if(traducao.contains(","))
			traducao = traducao.substring(0, traducao.indexOf(","));
		return traducao;
	}

}
