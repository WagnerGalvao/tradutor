import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Testes da Classe Tradutor
 * 
 * Hands-on: Tradutor com TDD
 * 
 * TDD � Desenvolvimento de Software Guiado pelo Testes por Instituto
 * Tecnol�gico da Aeron�utica
 * 
 * @author Wagner Galv�o galvao.wagner@outlook.com.br
 * @since 1.0
 * @version 1.0
 *
 */
public class TesteTradutor {

	private Tradutor tradutor;
	
	@Before
	public void novoTradutor() {
		tradutor = new Tradutor();
	};
	
	@Test
	public void tradutorSemPalavras() {
		assertTrue(tradutor.estaVazia());
	}

	@Test
	public void umaTraducao() {
		tradutor.adicionarTraducao("bom", "good");
		assertFalse(tradutor.estaVazia());
		assertEquals("good", tradutor.traduzir("bom"));
	};

	@Test
	public void duasTraducoes() {
		tradutor.adicionarTraducao("bom", "good");
		tradutor.adicionarTraducao("mau", "bad");
		assertEquals("good", tradutor.traduzir("bom"));
		assertEquals("bad", tradutor.traduzir("mau"));
	};

	@Test
	public void duasTraducoesMesmaPalavra() {
		tradutor.adicionarTraducao("bom", "good");
		tradutor.adicionarTraducao("bom", "nice");
		assertEquals("good, nice", tradutor.traduzir("bom"));
	};

	@Test
	public void umaFrase() {
		tradutor.adicionarTraducao("guerra", "war");
		tradutor.adicionarTraducao("�", "is");
		tradutor.adicionarTraducao("ruim", "bad");
		assertEquals("war is bad", tradutor.traduzirFrase("guerra � ruim"));
	};

	@Test
	public void umaFraseComDuasTraducoesDaMesmaPalavra() {
		tradutor.adicionarTraducao("paz", "peace");
		tradutor.adicionarTraducao("�", "is");
		tradutor.adicionarTraducao("bom", "good");
		tradutor.adicionarTraducao("bom", "nice");
		assertEquals("peace is good", tradutor.traduzirFrase("paz � bom"));
	};

}
